import java.util.*;

/** Quaternions. Basic operations. */
public class Quaternion {

	// Vajalikud muudatused:
	// Meetod valueOf peaks vigase sisendi korral seda sisendit ennast ka
	// veateates peegeldama, et kasutaja saaks aru, millisest stringist on jutt.
	// Meetod inverse kontrollib nulliga v�rdumist valesti (reaalarve ei saa ==
	// abil v�rrelda), kasutage �ra isZero. Ka meetodid divideBy... peaksid
	// nulliga jagamist kontrollima. Meetod hashCode annab s�mmeetria t�ttu
	// liiga kergesti kollisioone

	// Your fields here!
	private double r, i, j, k;

	/**
	 * Constructor from four double values.
	 * 
	 * @param a
	 *            real part
	 * @param b
	 *            imaginary part i
	 * @param c
	 *            imaginary part j
	 * @param d
	 *            imaginary part k
	 */
	public Quaternion(double a, double b, double c, double d) {
		r = a;
		i = b;
		j = c;
		k = d;
	}

	/**
	 * Real part of the quaternion.
	 * 
	 * @return real part
	 */
	public double getRpart() {
		return r;
	}

	/**
	 * Imaginary part i of the quaternion.
	 * 
	 * @return imaginary part i
	 */
	public double getIpart() {
		return i;
	}

	/**
	 * Imaginary part j of the quaternion.
	 * 
	 * @return imaginary part j
	 */
	public double getJpart() {
		return j;
	}

	/**
	 * Imaginary part k of the quaternion.
	 * 
	 * @return imaginary part k
	 */
	public double getKpart() {
		return k;
	}

	/**
	 * Conversion of the quaternion to the string.
	 * 
	 * @return a string form of this quaternion: "a+bi+cj+dk" (without any
	 *         brackets)
	 */
	@Override
	public String toString() {
		// ma ei ole kindel, kas "i, j, k" t�hed peab ka l�ppu panema. tundub,
		// et
		// testid l�bib m�lemal juhul
		StringBuilder sb = new StringBuilder();
		sb.append(r);
		sb.append("+");
		sb.append(i);
		sb.append("i+");
		sb.append(j);
		sb.append("j+");
		sb.append(k);
		sb.append("k");

		return sb.toString();
	}

	/**
	 * Conversion from the string to the quaternion. Reverse to
	 * <code>toString</code> method.
	 * 
	 * @throws IllegalArgumentException
	 *             if string s does not represent a quaternion (defined by the
	 *             <code>toString</code> method)
	 * @param s
	 *            string of form produced by the <code>toString</code> method
	 * @return a quaternion represented by string s
	 */
	public static Quaternion valueOf(String s) {
		// * Parandus: Meetod valueOf peaks vigase sisendi korral seda sisendit
		// ennast ka veateates peegeldama, et kasutaja saaks aru, millisest
		// stringist on jutt

		// "a+bi+cj+dk" - stringis peab olema 3 plussm�rki, proovin seda
		// numbrite k�ttesaamisel �ra kasutada
		try {
			String[] tykid = s.split("\\+");
			double r = Double.parseDouble(tykid[0]);
			// t�ht on vaja l�pust eemaldada, sellep�rast substring
			double i = Double.parseDouble(tykid[1].substring(0, tykid[1].length() - 1));
			double j = Double.parseDouble(tykid[2].substring(0, tykid[2].length() - 1));
			double k = Double.parseDouble(tykid[3].substring(0, tykid[3].length() - 1));

			// System.out.println("spliti t�kk r: " + r );
			// System.out.println("spliti t�kk i: " + i );
			// System.out.println("spliti t�kk j: " + j );
			// System.out.println("spliti t�kk k: " + k );
			return new Quaternion(r, i, j, k);
		} catch (Exception e) {
			throw new IllegalArgumentException("Sellest stringist ei saa quaternion-it teha: " + s);
		}
	}

	/**
	 * Clone of the quaternion.
	 * 
	 * @return independent clone of <code>this</code>
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Quaternion(r, i, j, k);
	}

	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @return true, if the real part and all the imaginary parts are (close to)
	 *         zero
	 */
	public boolean isZero() {
		// kuna double on "floating point", siis ta ei ole p�ris t�pselt 0
		double peaaeguNull = 0.0000001;
		if (Math.abs(r) < peaaeguNull && Math.abs(i) < peaaeguNull && Math.abs(j) < peaaeguNull
				&& Math.abs(k) < peaaeguNull) {
			return true;
		}
		return false;
	}

	/**
	 * Conjugate of the quaternion. Expressed by the formula
	 * conjugate(a+bi+cj+dk) = a-bi-cj-dk
	 * 
	 * @return conjugate of <code>this</code>
	 */
	public Quaternion conjugate() {
		return new Quaternion(r, -i, -j, -k);
	}

	/**
	 * Opposite of the quaternion. Expressed by the formula opposite(a+bi+cj+dk)
	 * = -a-bi-cj-dk
	 * 
	 * @return quaternion <code>-this</code>
	 */
	public Quaternion opposite() {
		return new Quaternion(-r, -i, -j, -k);
	}

	/**
	 * Sum of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) +
	 * (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
	 * 
	 * @param q
	 *            addend
	 * @return quaternion <code>this+q</code>
	 */
	public Quaternion plus(Quaternion q) {
		double a = r + q.r;
		double b = i + q.i;
		double c = j + q.j;
		double d = k + q.k;

		return new Quaternion(a, b, c, d);
	}

	/**
	 * Product of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) *
	 * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
	 * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
	 * 
	 * @param q
	 *            factor
	 * @return quaternion <code>this*q</code>
	 */
	public Quaternion times(Quaternion q) {
		double a = r * q.r - i * q.i - j * q.j - k * q.k;
		double b = r * q.i + i * q.r + j * q.k - k * q.j;
		double c = r * q.j - i * q.k + j * q.r + k * q.i;
		double d = r * q.k + i * q.j - j * q.i + k * q.r;

		return new Quaternion(a, b, c, d);
	}

	/**
	 * Multiplication by a coefficient.
	 * 
	 * @param r
	 *            coefficient
	 * @return quaternion <code>this*r</code>
	 */
	public Quaternion times(double r) {
		double a = this.r * r;
		double b = i * r;
		double c = j * r;
		double d = k * r;

		return new Quaternion(a, b, c, d);
	}

	/**
	 * Inverse of the quaternion. Expressed by the formula 1/(a+bi+cj+dk) =
	 * a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i +
	 * ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
	 * 
	 * @return quaternion <code>1/this</code>
	 */
	public Quaternion inverse() {
		// * Parandus: Meetod inverse kontrollib nulliga v�rdumist valesti
		// (reaalarve ei saa == abil v�rrelda), kasutage �ra isZero
		if (this.isZero()) {
			throw new RuntimeException("nulliga ei saa jagada");
		}

		double korduvTehe = r * r + i * i + j * j + k * k;

		double a = r / korduvTehe;
		double b = -i / korduvTehe;
		double c = -j / korduvTehe;
		double d = -k / korduvTehe;

		return new Quaternion(a, b, c, d);
	}

	/**
	 * Difference of quaternions. Expressed as addition to the opposite.
	 * 
	 * @param q
	 *            subtrahend
	 * @return quaternion <code>this-q</code>
	 */
	public Quaternion minus(Quaternion q) {
		return q.opposite().plus(this);
	}

	/**
	 * Right quotient of quaternions. Expressed as multiplication to the
	 * inverse.
	 * 
	 * @param q
	 *            (right) divisor
	 * @return quaternion <code>this*inverse(q)</code>
	 */
	public Quaternion divideByRight(Quaternion q) {
		// * Parandus: Nulliga jagamise kontroll
		if (q.isZero()) {
			throw new RuntimeException("nulliga ei saa jagada");
		}
		return times(q.inverse());
	}

	/**
	 * Left quotient of quaternions.
	 * 
	 * @param q
	 *            (left) divisor
	 * @return quaternion <code>inverse(q)*this</code>
	 */
	public Quaternion divideByLeft(Quaternion q) {
		// * Parandus: Nulliga jagamise kontroll
		if (this.isZero()) {
			throw new RuntimeException("nulliga ei saa jagada");
		}
		return q.inverse().times(this);
	}

	/**
	 * Equality test of quaternions. Difference of equal numbers is (close to)
	 * zero.
	 * 
	 * @param qo
	 *            second quaternion
	 * @return logical value of the expression <code>this.equals(qo)</code>
	 */
	@Override
	public boolean equals(Object qo) {
		// siin peaks olema mitu v�imalust, proovin kasutada minus() meetodit
		Quaternion q2 = (Quaternion) qo;

		if (q2.minus(this).isZero()) {
			return true;
		}
		return false;
	}

	/**
	 * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
	 * 
	 * @param q
	 *            factor
	 * @return dot product of this and q
	 */
	public Quaternion dotMult(Quaternion q) {
		// kasutab eespool olevaid conjugate, times ja plus meetodeid
		Quaternion esimPool = q.conjugate().times(this);
		Quaternion teinePool = this.conjugate().times(q);

		return (esimPool.plus(teinePool)).times(.5);
	}

	/**
	 * Integer hashCode has to be the same for equal objects.
	 * 
	 * @return hashcode
	 */
	@Override
	public int hashCode() {
		// * Parandus: Meetod hashCode annab s�mmeetria t�ttu liiga kergesti
		// kollisioone
		
		// kasutada tuleks algarve, siis peaks olema suurem v�imalus, et tulemus
		// tuleb unikaalne

		// http://www.javaranch.com/journal/2002/10/equalhash.html
		// long bits = Double.doubleToLongBits(var);
		// var_code = (int)(bits ^ (bits >>> 32));

		int tulemus = 7;
		long bits = Double.doubleToLongBits(r * 59 / i / 43);
		tulemus = tulemus * (int)(bits ^ (bits >>> 32));
		long bits2 = Double.doubleToLongBits(j * 13 / k / 17);
		tulemus = tulemus * (int)(bits2 ^ (bits2 >>> 32));
		long bits3 = Double.doubleToLongBits(i * 79 / k / 73);
		tulemus = tulemus * (int)(bits3 ^ (bits3 >>> 32));
		
		tulemus = (int) (j * k * 17 * tulemus) / 23;
		return tulemus;
	}

	/**
	 * Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) =
	 * Math.sqrt(a*a+b*b+c*c+d*d)
	 * 
	 * @return norm of <code>this</code> (norm is a real number)
	 */
	public double norm() {
		return Math.sqrt(r * r + i * i + j * j + k * k);
	}

	/**
	 * Main method for testing purposes.
	 * 
	 * @param arg
	 *            command line parameters
	 */
	public static void main(String[] arg) {
		// minu testid
		// Quaternion proov = new Quaternion(1., 2., 3., 4.);
		// Quaternion klooniProov = null;
		// try {
		// klooniProov = (Quaternion) proov.clone();
		// } catch (CloneNotSupportedException e1) {
		// System.out.println("kloonimine ei �nnestu");
		// }
		// System.out.println("quat : " + proov.toString());
		// System.out.println("kloon: " + klooniProov.toString());

		// need testid juba olid olemas
		Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
		// Quaternion arv1 = new Quaternion(0., 0, 0., 0.);
		if (arg.length > 0)
			arv1 = valueOf(arg[0]);
		System.out.println("first: " + arv1.toString());
		System.out.println("real: " + arv1.getRpart());
		System.out.println("imagi: " + arv1.getIpart());
		System.out.println("imagj: " + arv1.getJpart());
		System.out.println("imagk: " + arv1.getKpart());
		System.out.println("isZero: " + arv1.isZero());
		System.out.println("conjugate: " + arv1.conjugate());
		System.out.println("opposite: " + arv1.opposite());
		System.out.println("hashCode: " + arv1.hashCode());
		Quaternion res = null;
		try {
			res = (Quaternion) arv1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		System.out.println("clone equals to original: " + res.equals(arv1));
		System.out.println("clone is not the same object: " + (res != arv1));
		System.out.println("hashCode: " + res.hashCode());
		res = valueOf(arv1.toString());
		System.out.println("string conversion equals to original: " + res.equals(arv1));
		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
		if (arg.length > 1)
			arv2 = valueOf(arg[1]);
		System.out.println("second: " + arv2.toString());
		System.out.println("hashCode: " + arv2.hashCode());
		System.out.println("equals: " + arv1.equals(arv2));
		res = arv1.plus(arv2);
		System.out.println("plus: " + res);
		System.out.println("times: " + arv1.times(arv2));
		System.out.println("minus: " + arv1.minus(arv2));
		double mm = arv1.norm();
		System.out.println("norm: " + mm);
		System.out.println("inverse: " + arv1.inverse());
		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
		System.out.println("dotMult: " + arv1.dotMult(arv2));
	}
}
// end of file
